<?php

namespace Drupal\media_webdam\Tests;

use Drupal\simpletest\KernelTestBase;

/**
 * Placeholder test. Delete when there are real tests.
 *
 * @group media_webdam
 */
class PlaceholderTest extends KernelTestBase {

  /**
   * A test that always returns true.
   */
  public function testPlaceholder() {
    $this->assertTrue(TRUE);
  }

}
